// Soal 1

// Release 0

class Animal {
  // Code class di sini
  constructor(name) {
    this.name = name; 
    this.legs = 4;
    this.cold_blooded = false;
  }
}
var sheep = new Animal("shaun");

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

// Release 1

// Code class Ape dan class Frog di sini

class Ape extends Animal {
  constructor(name) {
    super(name);
    this.legs = 2;
  }

  yell(){
    return "Auooo";
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
    this.cold_blooded = true
  }
  jump() {
    return "hop hop";
  }
}

var sungokong = new Ape("kera sakti");
console.log(sungokong.yell()); // "Auooo"
 
var kodok = new Frog("buduk");
console.log(kodok.jump()); // "hop hop"

// Soal 2

class Clock {
  constructor ({template}) {
    this._template = template;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this._template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this._timer);
  };

  start() {
    this.render();
    this._timer = setInterval(this.render.bind(this), 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 
