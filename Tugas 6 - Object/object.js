// Soal 1

function arrayToObject(arr) {
  // Code di sini 
  
  var now = new Date();
  var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
  var length = arr.length;
  var object = {};

  for (var i = 0;i<length;i++) {
    var data = {};
    var number = i+1;
    var Fullname = number+". " + arr[i][0] + " " + arr[i][1];
    
    var umur = "";
    if (arr[i][3] == undefined || arr[i][3] > thisYear) {
      umur = "Invalid Birth Year";
    } else {
      umur = thisYear - arr[i][3] ;
    }
    
    data.firstName = arr[i][0];
    data.lastNama = arr[i][1];
    data.gender = arr[i][2];
    data.age = umur;

    object[Fullname] = data;

  }

  return object;
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
console.log(arrayToObject(people)) ;
/*
  1. Bruce Banner: { 
      firstName: "Bruce",
      lastName: "Banner",
      gender: "male",
      age: 45
  }
  2. Natasha Romanoff: { 
      firstName: "Natasha",
      lastName: "Romanoff",
      gender: "female".
      age: "Invalid Birth Year"
  }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
console.log(arrayToObject(people2)) ;
/*
  1. Tony Stark: { 
      firstName: "Tony",
      lastName: "Stark",
      gender: "male",
      age: 40
  }
  2. Pepper Pots: { 
      firstName: "Pepper",
      lastName: "Pots",
      gender: "female".
      age: "Invalid Birth Year"
  }
*/

// Error case 
console.log(arrayToObject([])); // ""


// Soal 2
function shoppingTime(memberId, money) {
  // you can only write your code here!

  if (memberId != '' && memberId != undefined) {
    
    if (money >= 50000) {
      var object = {};
      object["memberId"] = memberId;
      object["money"] = money;
      var purchased = [];
      if (money >= 1500000) {
        purchased.push("Sepatu Stacattu");
        money = money - 1500000;
      }
      if (money >= 500000) {
        purchased.push("Baju Zoro");
        money = money - 500000;
      } 
      if (money >= 250000) {
        purchased.push("Baju H&N");
        money = money - 250000;
      } 
      if (money >= 175000) {
        purchased.push("Sweater Uniklooh");
        money = money - 175000;
      } 
      if (money >= 50000) {
        purchased.push("Casing Handphone");
        money = money - 50000;
      }
      
      var kembalian = money;

      object["ListPurchased"] = purchased;
      object["Change Money"] = kembalian;
      return object;

    } else {
      return "Mohon maaf, uang tidak cukup";

    }

  } else {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";

  }
}
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here

  var result = [];
  
  for (var i= 0; i< arrPenumpang.length;i++) {
  
    var result2 = {};
    var start = rute.indexOf(arrPenumpang[i][1]);
    var finish = rute.indexOf(arrPenumpang[i][2]);
    var distance = finish-start;
    var pay = distance*2000;

    result2.penumpang = arrPenumpang[i][0];
    result2.naikDari = arrPenumpang[i][1];
    result2.tujuan = arrPenumpang[i][2];
    result2.bayar = pay;

    result.push(result2);

  }

  return result;

}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]

// ---------