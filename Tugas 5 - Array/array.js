// Soal 1

function range(startNum, finishNum){
  
  var array = [];

  if (startNum <= finishNum){
    while (startNum <= finishNum) {
      array.push(startNum);
      startNum++;
    }

  } else if (startNum > finishNum) {
    while (startNum >= finishNum) {
      array.push(startNum);
      startNum--;
    }

  } else {
    array.push(-1);
  }

  return array
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

// Soal 2

function rangeWithStep (startNum,finishNum,step=1) {
  
  var array = [];
  
  if (startNum <= finishNum){
    while (startNum <= finishNum) {
      array.push(startNum);
      startNum +=step;
    }

  } else if (startNum > finishNum) {
    while (startNum >= finishNum) {
      array.push(startNum);
      startNum -= step;
    }

  } else {
    array.push(-1);
  }
  
  return array;
}


console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3

function sum (startNum,finishNum,step=1) {
  
  function rangeWithStep (startNum,finishNum,step=1) {
    
    var array = [];
    
    if (startNum <= finishNum){
      while (startNum <= finishNum) {
        array.push(startNum);
        startNum +=step;
      }

    } else if (startNum > finishNum) {
      while (startNum >= finishNum) {
        array.push(startNum);
        startNum -= step;
      }

    } else if (finishNum == undefined) {
      array.push(startNum);

    } else if (startNum == undefined) {
      array.push(0);
    }

    return array
  }

  var arr = rangeWithStep (startNum,finishNum,step);

  var jumlah = 0;
  
  if (startNum != undefined) {
    jumlah = arr.reduce(function(a,b){return a+b});
  }

  return jumlah
}

console.log(sum(1,10)); // 55
console.log(sum(5,50,2));
console.log(sum(15,10));
console.log(sum(20,10,2));
console.log(sum(1));
console.log(sum());

// Soal 4

function dataHandling(input) {
  
  var result = ""
  var index = 0;
  
  while (index < input.length) {
  
    result = result + '\n' + "Nomor ID: " + input[index][0] + '\n' + "Nama Lengkap: " + input[index][1]  + '\n' + "TTL: " + input[index][2] + " " + input [index][3] + '\n' + "Hobi: " + input[index][4]  + '\n'
    index++;
  }

  return result
}

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log(dataHandling(input));

// Soal 5

function balikKata(text) {
  
  var reverse = "";
  var length = text.length - 1 ;
  
  while (length >= 0){
    reverse = reverse + text[length];
    length --;
  }

  return reverse;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 

// Soal 6

function dataHandling2(input) {

  var result = ""
  
  var nama = input[1];
  nama = nama + " Elsharawy";

  var tempat = input[2];
  tempat = "Provinsi " + tempat;

  input.splice(1,2,nama,tempat);
  input.splice(4,1,"SMA Internasional Metro");

  result = result + '\n' + input + '\n'

  var date = input[3];
  date = date.split("/");

  switch(date[1]){
    case '01': { result = result + "Januari" + '\n'; break;}
    case '02': { result = result + "Februari" + '\n'; break;}
    case '03': { result = result + "Maret" + '\n'; break;}
    case '04': { result = result + "April" + '\n'; break;} 
    case '05': { result = result + "Mei" + '\n'; break;}
    case '06': { result = result + "Juni" + '\n'; break;}
    case '07': { result = result + "Juli" + '\n'; break;}
    case '08': { result = result + "Agustus" + '\n'; break;}
    case '09': { result = result + "September" + '\n'; break;}
    case '10': { result = result + "Oktober" + '\n'; break;}
    case '11': { result = result + "November" + '\n'; break;}
    case '12': { result = result + "Desember" + '\n'; break;}
    default : {result = result + "Bulan yang dimasukkan salah" + '\n'; break;}
  }

  date.sort(function(a,b){return b-a});
  result = result + date + '\n';

  var date2 = input[3];
  date2 = date2.split("/");

  date2= date2.join("-");
  result = result + date2 + '\n';

  var name = input[1];
  // console.log(typeof name);

  name = name.slice(0,15);
  result = result + name + '\n';
  // console.log(name.length);

  return result
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log(dataHandling2(input));

// Testing