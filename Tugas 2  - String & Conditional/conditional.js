// Tugas 2 - Conditional

// Soal 1
// If-Else
// Werewolf
var nama = "";
var peran = "";

if(nama == ""){
  console.log("Nama Pemain Harus Diisi!!!");
} else {
  if (peran == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game");
  } else {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    if (peran == "Penyihir") {
      console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran == "Guard") {
      console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if (peran == "Werewolf"){
      console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    } else {
      console.log("Peran tidak tersedia");
    };
  }
}

// Soal 2
// Switch case 

var hari = 21; 
var bulan = 1; 
var tahun = 1945;



switch(true){
  case (hari >= 1 && hari <= 31):{ hari = hari; break;}
  default : {console.log("Tanggal yang dimasukkan salah"); break;}
}

switch(bulan) {
  case 1: { bulan = "Januari"; break; }
  case 2: { bulan = "Februari"; break; }
  case 3: { bulan = "Maret"; break; }
  case 4: { bulan = "April"; break; } 
  case 5: { bulan = "Mei"; break; }
  case 6: { bulan = "Juni"; break; }
  case 7: { bulan = "Juli"; break; }
  case 8: { bulan = "Agustus"; break; }
  case 9: { bulan = "September"; break; }
  case 10: { bulan = "Oktober"; break; }
  case 11: { bulan = "November"; break; }
  case 12: { bulan = "Desember"; break; }
  default : {console.log("Bulan yang dimasukkan salah"); break;}
}

switch(true){
  case (tahun >= 1900 && tahun <= 2200) : {tahun = tahun; break;}
  default : {console.log("Tahun yang dimasukkan salah"); break;}
}

console.log(hari + " " + bulan + " " + tahun)