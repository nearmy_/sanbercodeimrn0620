// Tugas 3 - Looping

// Soal 1
// While

// Menghitung maju

console.log ("LOOPING PERTAMA");

var angka = 2

while (angka <= 20) {
  console.log (angka + " - I Love Coding");
  angka += 2 ;
}

// Menghitung mundur

console.log ("LOOPING KEDUA");

var angka = 20

while (angka >= 2) {
  console.log (angka + " - I Will Become a Mobile Developer");
  angka -= 2 ;
}

// Soal 2
// For

for (var angka = 1; angka <= 20; angka++) {
  if (angka % 2 == 0) {
    console.log (angka + " - Berkualitas")
  } else if (angka % 3 == 0) {
    console.log (angka + " - I Love Coding")
  } else {
    console.log (angka + " - Santai")
  }
}

// Soal 3
// Membuat persegi panjang #

for (var lebar = 1; lebar <= 4; lebar++) {
  var persegiPanjang = ""
  for (var panjang = 1; panjang <=8; panjang++) {
    persegiPanjang = persegiPanjang + "#"
  }
  console.log (persegiPanjang)
}

// Soal 4
// Membuat tangga

var tangga = ""

for (var tinggi = 1; tinggi <= 7; tinggi++) {
  var tangga = tangga + "#"
  console.log (tangga)
}

// Soal 5
// Membuat papan catur

for (var vertikal = 1; vertikal <= 8; vertikal++) {
  var papan = ""
  for (var horisontal = 1; horisontal <= 8; horisontal++) {
    if (vertikal % 2 == 1) {
      if (horisontal % 2 == 1) {
        papan = papan += " "
      } else {
        papan = papan += "#"
      }
    } else {
      if (horisontal % 2 == 0) {
        papan = papan += " "
      } else {
        papan = papan += "#"
      }
    }
  }
  console.log (papan)
}